//1
document.getElementById('startButton').addEventListener('click', function() {
   setTimeout(function() {
       document.getElementById('statusDiv').textContent = 'Operation completed';
   }, 3000);
});


//2
window.onload = function() {
   let countdownDiv = document.getElementById('countdownDiv');
   let countdown = 10;

   let intervalId = setInterval(function() {
       countdownDiv.textContent = countdown;
       countdown--;

       if (countdown < 0) {
           clearInterval(intervalId);
           countdownDiv.textContent = 'Countdown completed';
       }
   }, 1000);
};
